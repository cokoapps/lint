# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.0](https://gitlab.coko.foundation/cokoapps/lint/compare/v2.0.1...v2.1.0) (2024-02-26)


### Features

* add cli command for lint checks ([c912ce1](https://gitlab.coko.foundation/cokoapps/lint/commit/c912ce1343ccf92668dfa197f01bd6a9f82043a9))


### Bug Fixes

* add bin entry to package.json for cli ([3ecb2f5](https://gitlab.coko.foundation/cokoapps/lint/commit/3ecb2f5aa35039ff4e06d54782e3b49a9316ff48))

### [2.0.1](https://gitlab.coko.foundation/cokoapps/lint/compare/v2.0.0...v2.0.1) (2023-02-23)


### Bug Fixes

* **eslint:** extend ESlint rules to files with the .jsx extension ([0bc0104](https://gitlab.coko.foundation/cokoapps/lint/commit/0bc01041bf366b757bd3aa06a279c74346bb42cf))

## [2.0.0](https://gitlab.coko.foundation/cokoapps/lint/compare/v1.4.0...v2.0.0) (2022-03-21)


### ⚠ BREAKING CHANGES

* husky needs a migration to hook files

* upgrade dependencies ([7be266a](https://gitlab.coko.foundation/cokoapps/lint/commit/7be266af0d829af54b2d909040643c8439802d2e))

## [1.4.0](https://gitlab.coko.foundation///compare/v1.3.0...v1.4.0) (2021-01-09)


### Features

* **eslint:** allow devdepencies in dev folder, ignore dist folder ([55b30cc](https://gitlab.coko.foundation///commit/55b30cc4d4ca764339771a26354a783cb8690de6))

## [1.3.0](https://gitlab.coko.foundation///compare/v1.2.0...v1.3.0) (2020-12-13)


### Features

* **eslint:** make console.log an error ([fa4aca8](https://gitlab.coko.foundation///commit/fa4aca8d4e83b638e74da5acf3c98516e6e79ee6))
* **stylelint:** add build to ignore patterns ([efe8e1f](https://gitlab.coko.foundation///commit/efe8e1f7c16145138335b673f4701cbaaa6f6466))

## [1.2.0](https://gitlab.coko.foundation///compare/v1.1.0...v1.2.0) (2020-12-13)


### Features

* **eslint:** add padding lines to config ([4914531](https://gitlab.coko.foundation///commit/491453104237737f14111265fdef13c43ef978e9))

## [1.1.0](https://gitlab.coko.foundation///compare/v1.0.2...v1.1.0) (2020-10-28)


### Features

* **eslint:** add exceptions to prop types ([7f71744](https://gitlab.coko.foundation///commit/7f71744254db5ddf1357ea9b39901715968cb93d))
* **stylelint:** add empty lines rule ([0cf0bf1](https://gitlab.coko.foundation///commit/0cf0bf1f7e1e9cf2bdec5e21a656491fcba73a33))
* **stylelint:** add no-important and empty-line rules ([18575f4](https://gitlab.coko.foundation///commit/18575f49d3fefde34f8b60ca86622164321c0fb3))

### [1.0.2](https://gitlab.coko.foundation///compare/v1.0.1...v1.0.2) (2020-10-26)


### Bug Fixes

* **eslint:** fix eslint cypress not working ([871ccd7](https://gitlab.coko.foundation///commit/871ccd74568170c54e85a09e36ef755e9b765b6b))

### [1.0.1](https://gitlab.coko.foundation///compare/v1.0.0...v1.0.1) (2020-10-24)


### Bug Fixes

* **lintstaged:** change lintstaged config to new standard ([04113f1](https://gitlab.coko.foundation///commit/04113f1e98957fdaa07aed69c46cb03707d8d36c))

## [1.0.0](https://gitlab.coko.foundation///compare/v0.0.5...v1.0.0) (2020-10-24)


### Bug Fixes

* **eslint:** fix ignore patterns typo ([9d6660a](https://gitlab.coko.foundation///commit/9d6660ac846520f3bd0bf0b9b02f8236b32d44ca))

### [0.0.5](https://gitlab.coko.foundation///compare/v0.0.4...v0.0.5) (2020-10-24)


### Bug Fixes

* **lintstaged:** export lintstaged ([20a61b5](https://gitlab.coko.foundation///commit/20a61b577d6951aee02511b5ee398af306f6fae8))

### [0.0.4](https://gitlab.coko.foundation///compare/v0.0.3...v0.0.4) (2020-10-24)


### Features

* **eslint:** add env, parser and more dev folders ([af2e882](https://gitlab.coko.foundation///commit/af2e8827abb2b6f79c9aaca756882b1b9449fc4f))
* **eslint:** add ignorePatterns so that eslintignore is not necessary ([db723bf](https://gitlab.coko.foundation///commit/db723bfa6d03511580b6797ddbd65b3adc25b1e5))
* **lintstaged:** add lintstaged dependency and config ([f30641e](https://gitlab.coko.foundation///commit/f30641e4ea6aa63d12070a6479c5f5e4f16b2f00))

### [0.0.3](https://gitlab.coko.foundation///compare/v0.0.2...v0.0.3) (2020-10-24)


### Features

* **stylelint:** add stylelint dependencies and first config ([2608ed7](https://gitlab.coko.foundation///commit/2608ed7a274264d680fcc775d43846ff7d3d256a))

### [0.0.2](https://gitlab.coko.foundation///compare/v0.0.1...v0.0.2) (2020-03-26)


### Features

* **commitlint:** add commitizen cli support ([6520e86](https://gitlab.coko.foundation///commit/6520e86080df7f5bb5b727468058e6c44abaa6a3))
* **prettier:** export prettier config ([3bab416](https://gitlab.coko.foundation///commit/3bab41624380c59978a8381c5b07778a7968534a))

### 0.0.1 (2020-03-24)


### Features

* **eslint:** initial eslint config ([bbef432](https://gitlab.coko.foundation///commit/bbef43287df93d1263cd27c870abd4c44701631d))
