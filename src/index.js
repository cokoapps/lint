const commitizen = require('./commitizen')
const eslint = require('./eslint')
const prettier = require('./prettier')
const stylelint = require('./stylelint')
const lintstaged = require('./lintstaged')

module.exports = {
  commitizen,
  eslint,
  prettier,
  stylelint,
  lintstaged,
}
